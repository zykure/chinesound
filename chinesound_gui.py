#!/usr/bin/env python2
# -*- coding: utf8 -*-


import sys
import time
import argparse

from _chinesound_gui import *
from _chinesound import *

######################################################################

parser = argparse.ArgumentParser(description='Play funny sounds - endlessly!')

parser.add_argument('-d,--device', dest='device', action='store',
                    type=int, default=-1,
                    help='select audio device (use --list to see list of devices)')
parser.add_argument('--list', dest='list_devices', action='store_true',
                    help='list available audio devices')

parser.add_argument('-c,--config', dest='config', action='store',
                    help='load settings from config file')

args = parser.parse_args()

if args.list_devices:
    pyo.pa_list_devices()
    exit(0)

server = SoundServer()
server.boot(args.device)

time.sleep(2)

app = MyApp(args.config)
app.MainLoop()
