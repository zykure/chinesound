#!/usr/bin/env python2
# -*- coding: utf8 -*-

import sys, os
import ConfigParser

import wx
from wx import xrc

#import midi
import subprocess

from _chinesound import *

class MyApp(wx.App):

    TIMER_ID = 100
    REFRESH_RATE = 50

    def __init__(self, config=None):
        self.loadConfig = config
        self.generator = ToneGenerator()
        wx.App.__init__(self)

    def OnInit(self):
        self.res = xrc.XmlResource('_chinesound_gui.xrc')
        self.init_frame()

        self.toneCount = -1

        if self.loadConfig:
            self.loadFile(self.loadConfig)
        else:
            self.update()

        self.generator.start()

        return True

    def init_frame(self):
        self.frame = self.res.LoadFrame(None, 'MainFrame')
        self.timer = wx.Timer(self.frame, self.TIMER_ID)

        self.sliderSpeed            = xrc.XRCCTRL(self.frame, 'm_sliderSpeed')
        self.sliderVolume           = xrc.XRCCTRL(self.frame, 'm_sliderVolume')
        self.spinCtrlOutputs        = xrc.XRCCTRL(self.frame, 'm_spinCtrlOutputs')
        self.choiceRandomizer       = xrc.XRCCTRL(self.frame, 'm_choiceRandomizer')
        self.choiceScale            = xrc.XRCCTRL(self.frame, 'm_choiceScale')
        self.spinCtrlRoot           = xrc.XRCCTRL(self.frame, 'm_spinCtrlRoot')
        self.sliderSigma            = xrc.XRCCTRL(self.frame, 'm_sliderSigma')
        self.sliderBackshift        = xrc.XRCCTRL(self.frame, 'm_sliderBackshift')
        self.checkboxChannel1       = xrc.XRCCTRL(self.frame, 'm_checkBoxChannel1')
        self.spinCtrlBasetone1      = xrc.XRCCTRL(self.frame, 'm_spinCtrlBasetone1')
        self.checkboxChannel2       = xrc.XRCCTRL(self.frame, 'm_checkBoxChannel2')
        self.spinCtrlBasetone2      = xrc.XRCCTRL(self.frame, 'm_spinCtrlBasetone2')
        self.checkboxChannel3       = xrc.XRCCTRL(self.frame, 'm_checkBoxChannel3')
        self.spinCtrlBasetone3      = xrc.XRCCTRL(self.frame, 'm_spinCtrlBasetone3')

        self.labelSpeed             = xrc.XRCCTRL(self.frame, 'm_labelSpeed')
        self.labelVolume            = xrc.XRCCTRL(self.frame, 'm_labelVolume')
        self.labelSigma             = xrc.XRCCTRL(self.frame, 'm_labelSigma')
        self.labelBackshift         = xrc.XRCCTRL(self.frame, 'm_labelBackshift')
        self.labelRoot              = xrc.XRCCTRL(self.frame, 'm_labelRoot')
        self.labelBasetone1         = xrc.XRCCTRL(self.frame, 'm_labelBasetone1')
        self.labelBasetone2         = xrc.XRCCTRL(self.frame, 'm_labelBasetone2')
        self.labelBasetone3         = xrc.XRCCTRL(self.frame, 'm_labelBasetone3')

        for scale in SCALES.keys():
            self.choiceScale.Append(scale.capitalize())
        self.choiceScale.Select(0)

        for randomizer in RANDOMIZERS.keys():
            self.choiceRandomizer.Append(randomizer.capitalize())
        self.choiceRandomizer.Select(0)

        self.frame.Bind(wx.EVT_SCROLL,      self.setSpeed,          id=xrc.XRCID('m_sliderSpeed'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setVolume,         id=xrc.XRCID('m_sliderVolume'))
        self.frame.Bind(wx.EVT_SPINCTRL,    self.setOutputs,        id=xrc.XRCID('m_spinCtrlOutputs'))
        self.frame.Bind(wx.EVT_CHOICE,      self.setRandomizer,     id=xrc.XRCID('m_choiceRandomizer'))
        self.frame.Bind(wx.EVT_CHOICE,      self.setScale,          id=xrc.XRCID('m_choiceScale'))
        self.frame.Bind(wx.EVT_SPINCTRL,    self.setRoot,           id=xrc.XRCID('m_spinCtrlRoot'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setSigma,          id=xrc.XRCID('m_sliderSigma'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setBackshift,      id=xrc.XRCID('m_sliderBackshift'))
        self.frame.Bind(wx.EVT_CHECKBOX,    self.setChannels,       id=xrc.XRCID('m_checkBoxChannel1'))
        self.frame.Bind(wx.EVT_SPINCTRL,    self.setChannels,       id=xrc.XRCID('m_spinCtrlBasetone1'))
        self.frame.Bind(wx.EVT_CHECKBOX,    self.setChannels,       id=xrc.XRCID('m_checkBoxChannel2'))
        self.frame.Bind(wx.EVT_SPINCTRL,    self.setChannels,       id=xrc.XRCID('m_spinCtrlBasetone2'))
        self.frame.Bind(wx.EVT_CHECKBOX,    self.setChannels,       id=xrc.XRCID('m_checkBoxChannel3'))
        self.frame.Bind(wx.EVT_SPINCTRL,    self.setChannels,       id=xrc.XRCID('m_spinCtrlBasetone3'))


        self.radioButtonToneSine    = xrc.XRCCTRL(self.frame, 'm_radioBtnToneSine')
        self.radioButtonToneSaw     = xrc.XRCCTRL(self.frame, 'm_radioBtnToneSaw')
        self.radioButtonToneSum     = xrc.XRCCTRL(self.frame, 'm_radioBtnToneSum')
        self.checkboxStaccato       = xrc.XRCCTRL(self.frame, 'm_checkBoxStaccato')
        self.checkboxShuffle        = xrc.XRCCTRL(self.frame, 'm_checkBoxShuffle')
        self.sliderToneSawDetune    = xrc.XRCCTRL(self.frame, 'm_sliderToneSawDetune')
        self.sliderToneSawBalance   = xrc.XRCCTRL(self.frame, 'm_sliderToneSawBalance')
        self.sliderToneSumRatio     = xrc.XRCCTRL(self.frame, 'm_sliderToneSumRatio')
        self.sliderToneSumIndex     = xrc.XRCCTRL(self.frame, 'm_sliderToneSumIndex')

        self.labelSawDetune         = xrc.XRCCTRL(self.frame, 'm_labelSawDetune')
        self.labelSawBalance        = xrc.XRCCTRL(self.frame, 'm_labelSawBalance')
        self.labelSumRatio          = xrc.XRCCTRL(self.frame, 'm_labelSumRatio')
        self.labelSumIndex          = xrc.XRCCTRL(self.frame, 'm_labelSumIndex')

        self.frame.Bind(wx.EVT_RADIOBUTTON, self.setTone,           id=xrc.XRCID('m_radioBtnToneSine'))
        self.frame.Bind(wx.EVT_RADIOBUTTON, self.setTone,           id=xrc.XRCID('m_radioBtnToneSaw'))
        self.frame.Bind(wx.EVT_RADIOBUTTON, self.setTone,           id=xrc.XRCID('m_radioBtnToneSum'))
        self.frame.Bind(wx.EVT_CHECKBOX,    self.setStaccato,       id=xrc.XRCID('m_checkBoxStaccato'))
        self.frame.Bind(wx.EVT_CHECKBOX,    self.setShuffle,        id=xrc.XRCID('m_checkBoxShuffle'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setTone,           id=xrc.XRCID('m_sliderToneSawDetune'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setTone,           id=xrc.XRCID('m_sliderToneSawBalance'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setTone,           id=xrc.XRCID('m_sliderToneSumRatio'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setTone,           id=xrc.XRCID('m_sliderToneSumIndex'))


        self.checkboxHarmonize      = xrc.XRCCTRL(self.frame, 'm_checkBoxHarmonize')
        self.checkboxDistort        = xrc.XRCCTRL(self.frame, 'm_checkBoxDistort')
        self.checkboxChorus         = xrc.XRCCTRL(self.frame, 'm_checkBoxChorus')
        self.checkboxReverb         = xrc.XRCCTRL(self.frame, 'm_checkBoxReverb')
        self.checkboxDelay          = xrc.XRCCTRL(self.frame, 'm_checkBoxDelay')

        self.frame.Bind(wx.EVT_CHECKBOX,    self.setEffects,        id=xrc.XRCID('m_checkBoxHarmonize'))
        self.frame.Bind(wx.EVT_CHECKBOX,    self.setEffects,        id=xrc.XRCID('m_checkBoxDistort'))
        self.frame.Bind(wx.EVT_CHECKBOX,    self.setEffects,        id=xrc.XRCID('m_checkBoxChorus'))
        self.frame.Bind(wx.EVT_CHECKBOX,    self.setEffects,        id=xrc.XRCID('m_checkBoxReverb'))
        self.frame.Bind(wx.EVT_CHECKBOX,    self.setEffects,        id=xrc.XRCID('m_checkBoxDelay'))


        self.spinCtrlHarmonizeDist  = xrc.XRCCTRL(self.frame, 'm_spinCtrlHarmonizeDist')
        self.sliderHarmonizeFeedback= xrc.XRCCTRL(self.frame, 'm_sliderHarmonizeFeedback')
        self.sliderHarmonizeWindow  = xrc.XRCCTRL(self.frame, 'm_sliderHarmonizeWindow')

        self.labelHarmonizeDist     = xrc.XRCCTRL(self.frame, 'm_labelHarmonizeDist')
        self.labelHarmonizeFeedback = xrc.XRCCTRL(self.frame, 'm_labelHarmonizeFeedback')
        self.labelHarmonizeWindow   = xrc.XRCCTRL(self.frame, 'm_labelHarmonizeWindow')

        self.frame.Bind(wx.EVT_SPINCTRL,    self.setEffects,        id=xrc.XRCID('m_spinCtrlHarmonizeDist'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setEffects,        id=xrc.XRCID('m_sliderHarmonizeFeedback'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setEffects,        id=xrc.XRCID('m_sliderHarmonizeWindow'))


        self.sliderDistortDrive     = xrc.XRCCTRL(self.frame, 'm_sliderDistortDrive')
        self.sliderDistortSlope     = xrc.XRCCTRL(self.frame, 'm_sliderDistortSlope')

        self.labelDistortDrive      = xrc.XRCCTRL(self.frame, 'm_labelDistortDrive')
        self.labelDistortSlope      = xrc.XRCCTRL(self.frame, 'm_labelDistortSlope')

        self.frame.Bind(wx.EVT_SCROLL,      self.setEffects,        id=xrc.XRCID('m_sliderDistortDrive'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setEffects,        id=xrc.XRCID('m_sliderDistortSlope'))


        self.sliderChorusDepth      = xrc.XRCCTRL(self.frame, 'm_sliderChorusDepth')
        self.sliderChorusFeedback   = xrc.XRCCTRL(self.frame, 'm_sliderChorusFeedback')
        self.sliderChorusBalance    = xrc.XRCCTRL(self.frame, 'm_sliderChorusBalance')

        self.labelChorusDepth       = xrc.XRCCTRL(self.frame, 'm_labelChorusDepth')
        self.labelChorusFeedback    = xrc.XRCCTRL(self.frame, 'm_labelChorusFeedback')
        self.labelChorusBalance     = xrc.XRCCTRL(self.frame, 'm_labelChorusBalance')

        self.frame.Bind(wx.EVT_SCROLL,      self.setEffects,        id=xrc.XRCID('m_sliderChorusDepth'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setEffects,        id=xrc.XRCID('m_sliderChorusFeedback'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setEffects,        id=xrc.XRCID('m_sliderChorusBalance'))


        self.sliderReverbSize       = xrc.XRCCTRL(self.frame, 'm_sliderReverbSize')
        self.sliderReverbDamp       = xrc.XRCCTRL(self.frame, 'm_sliderReverbDamp')
        self.sliderReverbBalance    = xrc.XRCCTRL(self.frame, 'm_sliderReverbBalance')

        self.labelReverbSize        = xrc.XRCCTRL(self.frame, 'm_labelReverbSize')
        self.labelReverbDamp        = xrc.XRCCTRL(self.frame, 'm_labelReverbDamp')
        self.labelReverbBalance     = xrc.XRCCTRL(self.frame, 'm_labelReverbBalance')

        self.frame.Bind(wx.EVT_SCROLL,      self.setEffects,        id=xrc.XRCID('m_sliderReverbSize'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setEffects,        id=xrc.XRCID('m_sliderReverbDamp'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setEffects,        id=xrc.XRCID('m_sliderReverbBalance'))


        self.sliderDelayTime        = xrc.XRCCTRL(self.frame, 'm_sliderDelayTime')
        self.sliderDelayFeedback    = xrc.XRCCTRL(self.frame, 'm_sliderDelayFeedback')

        self.labelDelayTime         = xrc.XRCCTRL(self.frame, 'm_labelDelayTime')
        self.labelDelayFeedback     = xrc.XRCCTRL(self.frame, 'm_labelDelayFeedback')

        self.frame.Bind(wx.EVT_SCROLL,      self.setEffects,        id=xrc.XRCID('m_sliderDelayTime'))
        self.frame.Bind(wx.EVT_SCROLL,      self.setEffects,        id=xrc.XRCID('m_sliderDelayFeedback'))


        self.toggleButtonPlayPause  = xrc.XRCCTRL(self.frame, 'm_toggleBtnPlayPause')
        self.buttonSave             = xrc.XRCCTRL(self.frame, 'm_buttonSave')
        self.buttonLoad             = xrc.XRCCTRL(self.frame, 'm_buttonLoad')
        self.buttonExport           = xrc.XRCCTRL(self.frame, 'm_buttonExport')
        self.buttonClear            = xrc.XRCCTRL(self.frame, 'm_buttonClear')
        self.buttonClose            = xrc.XRCCTRL(self.frame, 'm_buttonClear')

        self.frame.Bind(wx.EVT_TOGGLEBUTTON,self.playpause,         id=xrc.XRCID('m_toggleBtnPlayPause'))
        self.frame.Bind(wx.EVT_BUTTON,      self.save,              id=xrc.XRCID('m_buttonSave'))
        self.frame.Bind(wx.EVT_BUTTON,      self.load,              id=xrc.XRCID('m_buttonLoad'))
        self.frame.Bind(wx.EVT_BUTTON,      self.export,            id=xrc.XRCID('m_buttonExport'))
        self.frame.Bind(wx.EVT_BUTTON,      self.clear,             id=xrc.XRCID('m_buttonClear'))
        self.frame.Bind(wx.EVT_BUTTON,      self.close,             id=xrc.XRCID('m_buttonClose'))


        self.panelCurrentTone           = xrc.XRCCTRL(self.frame, 'm_panelCurrentTone')
        self.panelTone                  = xrc.XRCCTRL(self.frame, 'm_panelTone')
        self.panelHistogram             = xrc.XRCCTRL(self.frame, 'm_panelHistogram')
        self.panelFlow                  = xrc.XRCCTRL(self.frame, 'm_panelFlow')


        self.sliderToneRange            = xrc.XRCCTRL(self.frame, 'm_sliderToneRange')
        self.labelToneRangeLow          = xrc.XRCCTRL(self.frame, 'm_labelToneRangeLow')
        self.labelToneRangeHigh         = xrc.XRCCTRL(self.frame, 'm_labelToneRangeHigh')

        self.frame.Bind(wx.EVT_SCROLL,      self.setToneRange,      id=xrc.XRCID('m_sliderToneRange'))


        #self.frame.Bind(wx.EVT_PAINT, self.draw)
        self.frame.Bind(wx.EVT_TIMER, self.draw, self.timer)
        self.frame.Bind(wx.EVT_CLOSE, self.close)

        self.frame.SetDoubleBuffered(True)
        self.frame.Show()

        self.timer.Start(1000 / self.REFRESH_RATE)

    # class methods

    def update(self):
        self.setToneRange(None)
        self.setVolume(None)
        self.setScale(None)
        self.setRandomizer(None)
        self.setRoot(None)
        self.setSigma(None)
        self.setBackshift(None)
        self.setSpeed(None)
        self.setChannels(None)
        self.setOutputs(None)
        self.setTone(None)
        self.setStaccato(None)
        self.setShuffle(None)
        self.setEffects(None)

    def saveFile(self, filename):
        config = ConfigParser.RawConfigParser()

        config.add_section('Basic Settings')
        config.set('Basic Settings',        'speed',                self.generator.speed)
        config.set('Basic Settings',        'volume',               self.generator.volume)
        #config.set('Basic Settings',        'outputs',              self.generator.outputs)
        config.set('Basic Settings',        'scale',                self.generator.scalename)
        config.set('Basic Settings',        'randomizer',           self.generator.randomizer)
        config.set('Basic Settings',        'root',                 self.generator.root)
        config.set('Basic Settings',        'tonerange',            self.generator.tonerange)
        config.set('Basic Settings',        'sigma',                self.generator.sigma)
        config.set('Basic Settings',        'backshift',            self.generator.backshift)
        config.set('Basic Settings',        'channels',             self.generator.channels)

        config.add_section('Signal Generators')
        config.set('Signal Generators',     'tonesaw',              self.generator.toneSaw)
        config.set('Signal Generators',     'tonesaw_detune',       self.generator.toneSawDetune)
        config.set('Signal Generators',     'tonesaw_balance',      self.generator.toneSawBalance)
        config.set('Signal Generators',     'tonesum',              self.generator.toneSum)
        config.set('Signal Generators',     'tonesum_ratio',        self.generator.toneSumRatio)
        config.set('Signal Generators',     'tonesum_index',        self.generator.toneSumIndex)
        config.set('Signal Generators',     'staccato',             self.generator.staccato)
        config.set('Signal Generators',     'shuffle',              self.generator.shuffle)

        config.add_section('Effects')
        config.set('Effects',               'harmonize',            self.generator.harmonize)
        config.set('Effects',               'harmonize_dist',       self.generator.harmonizeDist)
        config.set('Effects',               'harmonize_feedback',   self.generator.harmonizeFeedback)
        config.set('Effects',               'harmonize_window',     self.generator.harmonizeWindow)
        config.set('Effects',               'distort',              self.generator.distort)
        config.set('Effects',               'distort_drive',        self.generator.distortDrive)
        config.set('Effects',               'distort_slope',        self.generator.distortSlope)
        config.set('Effects',               'chorus',               self.generator.chorus)
        config.set('Effects',               'chorus_depth',         self.generator.chorusDepth)
        config.set('Effects',               'chorus_feedback',      self.generator.chorusFeedback)
        config.set('Effects',               'chorus_balance',       self.generator.chorusBalance)
        config.set('Effects',               'reverb',               self.generator.reverb)
        config.set('Effects',               'reverb_size',          self.generator.reverbSize)
        config.set('Effects',               'reverb_damp',          self.generator.reverbDamp)
        config.set('Effects',               'reverb_balance',       self.generator.reverbBalance)
        config.set('Effects',               'delay',                self.generator.delay)
        config.set('Effects',               'delay_time',           self.generator.delayTime)
        config.set('Effects',               'delay_feedback',       self.generator.delayFeedback)

        with open(filename, 'wb') as configfile:
            config.write(configfile)

    def loadFile(self, filename):
            defaults = {
                'speed':                str(self.sliderSpeed.GetValue()),
                'volume':               str(self.sliderVolume.GetValue() / 100.),
                #'outputs':             str(self.spinCtrlOutputs.GetValue()),
                'channels':             str(OrderedDict([
                                            ( self.checkboxChannel1.GetValue(), self.spinCtrlBasetone1.GetValue() ),
                                            ( self.checkboxChannel2.GetValue(), self.spinCtrlBasetone2.GetValue() ),
                                            ( self.checkboxChannel3.GetValue(), self.spinCtrlBasetone3.GetValue() ),
                                        ])),
                'randomizer':           str(self.choiceRandomizer.GetString(self.choiceRandomizer.GetSelection()).lower()),
                'scale':                str(self.choiceScale.GetString(self.choiceScale.GetSelection()).lower()),
                'root':                 str(self.spinCtrlRoot.GetValue()),
                'tonerange':            str(self.sliderToneRange.GetValue()),
                'sigma':                str(self.sliderSigma.GetValue() / 100.),
                'backshift':            str(self.sliderBackshift.GetValue() / 100.),
                'tonesaw':              str(self.radioButtonToneSaw.GetValue()),
                'tonesaw_detune':       str(self.sliderToneSawDetune.GetValue() / 100.),
                'tonesaw_balance':      str(self.sliderToneSawBalance.GetValue() / 100.),
                'tonesum':              str(self.radioButtonToneSum.GetValue()),
                'tonesum_ratio':        str(self.sliderToneSumRatio.GetValue() / 100.),
                'tonesum_index':        str(self.sliderToneSumIndex.GetValue() / 100.),
                'staccato':             str(self.checkboxStaccato.GetValue()),
                'shuffle':              str(self.checkboxShuffle.GetValue()),
                'harmonize':            str(self.checkboxHarmonize.GetValue()),
                'harmonize_dist':       str(self.spinCtrlHarmonizeDist.GetValue()),
                'harmonize_feedback':   str(self.sliderHarmonizeFeedback.GetValue() / 100.),
                'harmonize_window':     str(self.sliderHarmonizeWindow.GetValue() / 100.),
                'distort':              str(self.checkboxDistort.GetValue()),
                'distort_drive':        str(self.sliderDistortDrive.GetValue() / 100.),
                'distort_slope':        str(self.sliderDistortSlope.GetValue() / 100.),
                'chorus':               str(self.checkboxChorus.GetValue()),
                'chorus_depth':         str(self.sliderChorusDepth.GetValue() / 100.),
                'chorus_feedback':      str(self.sliderChorusFeedback.GetValue() / 100.),
                'chorus_balance':       str(self.sliderChorusBalance.GetValue() / 100.),
                'reverb':               str(self.checkboxReverb.GetValue()),
                'reverb_size':          str(self.sliderReverbSize.GetValue() / 100.),
                'reverb_damp':          str(self.sliderReverbDamp.GetValue() / 100.),
                'reverb_balance':       str(self.sliderReverbBalance.GetValue() / 100.),
                'delay':                str(self.checkboxDelay.GetValue()),
                'delay_time':           str(self.sliderDelayTime.GetValue() / 100.),
                'delay_feedback':       str(self.sliderDelayTime.GetValue() / 100.),
            }
            config = ConfigParser.RawConfigParser(defaults)

            with open(filename, 'rb') as configfile:
                config.readfp(configfile)

                speed            = config.getint('Basic Settings',           'speed')
                volume           = config.getfloat('Basic Settings',         'volume')
                #outputs          = config.getint('Basic Settings',           'outputs')
                channels         = eval(config.get('Basic Settings',         'channels'))  # OrderedDict
                randomizer       = config.get('Basic Settings',              'randomizer')
                scalename        = config.get('Basic Settings',              'scale')
                root             = config.getint('Basic Settings',           'root')
                tonerange        = config.getint('Basic Settings',           'tonerange')
                sigma            = config.getfloat('Basic Settings',         'sigma')
                backshift        = config.getfloat('Basic Settings',         'backshift')

                toneSaw          = config.getboolean('Signal Generators',    'tonesaw')
                toneSawDetune    = config.getfloat('Signal Generators',      'tonesaw_detune')
                toneSawBalance   = config.getfloat('Signal Generators',      'tonesaw_balance')
                toneSum          = config.getboolean('Signal Generators',    'tonesum')
                toneSumRatio     = config.getfloat('Signal Generators',      'tonesum_ratio')
                toneSumIndex     = config.getfloat('Signal Generators',      'tonesum_index')
                staccato         = config.getboolean('Signal Generators',    'staccato')
                shuffle          = config.getboolean('Signal Generators',    'shuffle')

                harmonize        = config.getboolean('Effects',              'harmonize')
                harmonizeDist    = config.getfloat('Effects',                'harmonize_dist')
                harmonizeFeedback= config.getfloat('Effects',                'harmonize_feedback')
                harmonizeWindow  = config.getfloat('Effects',                'harmonize_window')
                distort          = config.getboolean('Effects',              'distort')
                distortDrive     = config.getfloat('Effects',                'distort_drive')
                distortSlope     = config.getfloat('Effects',                'distort_slope')
                chorus           = config.getboolean('Effects',              'chorus')
                chorusDepth      = config.getfloat('Effects',                'chorus_depth')
                chorusFeedback   = config.getfloat('Effects',                'chorus_feedback')
                chorusBalance    = config.getfloat('Effects',                'chorus_balance')
                reverb           = config.getboolean('Effects',              'reverb')
                reverbSize       = config.getfloat('Effects',                'reverb_size')
                reverbDamp       = config.getfloat('Effects',                'reverb_damp')
                reverbBalance    = config.getfloat('Effects',                'reverb_balance')
                delay            = config.getboolean('Effects',              'delay')
                delayTime        = config.getfloat('Effects',                'delay_time')
                delayFeedback    = config.getfloat('Effects',                'delay_feedback')

                isPaused = self.generator.paused
                self.generator.pause()

                self.sliderToneRange.SetValue(tonerange)
                self.sliderSpeed.SetValue(speed)
                self.sliderVolume.SetValue(volume * 100)
                #self.spinCtrlOutputs.SetValue(outputs)
                self.choiceRandomizer.SetSelection(self.choiceRandomizer.FindString(randomizer.capitalize()))
                self.choiceScale.SetSelection(self.choiceScale.FindString(scalename.capitalize()))
                self.spinCtrlRoot.SetValue(root)
                self.sliderSigma.SetValue(sigma * 100)
                self.sliderBackshift.SetValue(backshift * 100)
                self.checkboxChannel1.SetValue(channels.has_key(1))
                self.checkboxChannel2.SetValue(channels.has_key(2))
                self.checkboxChannel3.SetValue(channels.has_key(3))
                self.spinCtrlBasetone1.SetValue(channels.get(1, 0))
                self.spinCtrlBasetone2.SetValue(channels.get(2, 0))
                self.spinCtrlBasetone3.SetValue(channels.get(3, 0))

                if toneSaw:
                    self.radioButtonToneSaw.SetValue(True)
                elif toneSum:
                    self.radioButtonToneSum.SetValue(True)
                else:
                    self.radioButtonToneSine.SetValue(True)
                self.sliderToneSawDetune.SetValue(toneSawDetune * 100)
                self.sliderToneSawBalance.SetValue(toneSawBalance * 100)
                self.sliderToneSumRatio.SetValue(toneSumRatio * 100)
                self.sliderToneSumIndex.SetValue(toneSumIndex * 100)
                self.checkboxStaccato.SetValue(staccato)
                self.checkboxShuffle.SetValue(shuffle)

                self.checkboxHarmonize.SetValue(harmonize)
                self.spinCtrlHarmonizeDist.SetValue(harmonizeDist)
                self.sliderHarmonizeFeedback.SetValue(harmonizeFeedback * 100)
                self.sliderHarmonizeWindow.SetValue(harmonizeWindow * 100)

                self.checkboxDistort.SetValue(distort)
                self.sliderDistortDrive.SetValue(distortDrive * 100)
                self.sliderDistortSlope.SetValue(distortSlope * 100)

                self.checkboxChorus.SetValue(chorus)
                self.sliderChorusDepth.SetValue(chorusDepth * 100)
                self.sliderChorusFeedback.SetValue(chorusFeedback * 100)
                self.sliderChorusBalance.SetValue(chorusBalance * 100)

                self.checkboxReverb.SetValue(reverb)
                self.sliderReverbSize.SetValue(reverbSize * 100)
                self.sliderReverbDamp.SetValue(reverbDamp * 100)
                self.sliderReverbBalance.SetValue(reverbBalance * 100)

                self.checkboxDelay.SetValue(delay)
                self.sliderDelayTime.SetValue(delayTime * 100)
                self.sliderDelayFeedback.SetValue(delayFeedback * 100)

            self.update()
            if not isPaused:
                self.generator.play()

    def writeLilypond(self, filename):
        channels = self.generator.channels
        speed    = self.generator.speed
        history  = self.generator.history

        tracks = OrderedDict()
        for c in channels.keys():
            tracks[c] = []

        for tones in history:
            for c in channels.keys():
                tracks[c].append(tones[c])

        # TODO - use voices instead of separate staffs for channels

        document = []
        document.append('\\score {')
        document.append('  \\version "2.16.2"')
        document.append('  \\new ChoirStaff <<')
        for track in tracks.values():
            notes = { 'bass': [], 'treble': [] }
            for t in track:
                note = "%s4" % self.generator.tone_name(t)
                if t >= 9:  # anything above c
                    notes['treble'].append(note)
                    notes['bass'].append("r4")
                else:
                    notes['treble'].append("r4")
                    notes['bass'].append(note)
            document.append('    \\new GrandStaff <<')
            key = self.generator.tone_name(self.generator.root)  # FIXME - strip off ' etc. if needed
            for clef,notes in notes.items():
                document.append('      \\new Staff {')
                document.append('        \\tempo 4 = %d' % speed)
                document.append('        \\clef %s' % clef)
                if "major" in self.generator.scalename:
                    document.append('        \\key %s \\major' % key)
                elif "minor" in self.generator.scalename:
                    document.append('        \\key %s \\minor' % key)
                document.append('        \\relative c\'')
                document.append('        ' + ' '.join(notes))
                document.append('      }')
            document.append('    >>')
        document.append('  >>')
        document.append('  \\layout {}')
        document.append('  \\midi {}')
        document.append('}')

        with open(filename, 'wb') as outputfile:
            outputfile.write("\n".join(document))
            print "Exported to '%s'" % filename

        try:
            proc = subprocess.call(["lilypond", filename])
            print "Lilypond run successful!"
        except:
            print "Error when running Lilypond (error %d)!" % proc.returncode

    #def writeMIDI(self, filename):
    #    channels = self.generator.channels
    #    speed    = self.generator.speed
    #    history  = self.generator.history
    #
    #    ticklen  = int(self.generator.bpm2length(2*speed) * 1000)  # milliseconds
    #
    #    tracks = [ midi.Track() for c in range(channels) ]
    #    for track in tracks:
    #        track.append(midi.SetTempoEvent(tick=0, bpm=speed))
    #
    #    lastPitch = [ None for t in tracks ]
    #
    #    tick = 0
    #    for tones in history:
    #        for c in range(channels):
    #            track = tracks[c]
    #            pitch = int(tones[c] + 69)  # MDII: a = A4 = #69
    #            if not lastPitch[c] or lastPitch[c] != pitch:
    #                track.append(midi.NoteOnEvent(tick=tick, velocity=100, pitch=pitch))
    #            lastPitch[c] = pitch
    #        tick = ticklen  # only first note starts at 0
    #
    #    pattern = midi.Pattern()
    #    for track in tracks:
    #        track.append(midi.EndOfTrackEvent(tick=tick))
    #        pattern.append(track)
    #
    #    midi.write_midifile(filename, pattern)
    #    print "Exported to '%s'" % filename

    ## event handlers

    def save(self, event):
        dlg = wx.FileDialog(self.frame,
            message="Save settings...",
            defaultDir=os.getcwd(),
            defaultFile="chinesound.cfg",
            wildcard="Settings files (*.cfg)|*.cfg|All files (*.*)|*.*",
            style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT
        )

        if dlg.ShowModal() == wx.ID_OK:
            self.saveFile(dlg.GetPath())

    def load(self, event):
        dlg = wx.FileDialog(self.frame,
            message="Load settings...",
            defaultDir=os.getcwd(),
            defaultFile="chinesound.cfg",
            wildcard="Settings files (*.cfg)|*.cfg|All files (*.*)|*.*",
            style=wx.FD_OPEN
        )

        if dlg.ShowModal() == wx.ID_OK:
            self.loadFile(dlg.GetPath())

    def export(self, event):
        dlg = wx.FileDialog(self.frame,
            message="Export to Lilypond file...",
            defaultDir=os.getcwd(),
            defaultFile="chinesound.ly",
            wildcard="Lilypond source files (*.ly)|*.ly|All files (*.*)|*.*",
            style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT
        )

        if dlg.ShowModal() == wx.ID_OK:
            self.writeLilypond(dlg.GetPath())

    def playpause(self, event):
        if self.toggleButtonPlayPause.GetValue():
            self.generator.play()
        else:
            self.generator.pause()

    def close(self, event):
        self.timer.Stop()

        self.generator.stop()
        self.generator.join()

        self.frame.Destroy()

    def clear(self, event):
        self.generator.clear()

    def updateLabels(self):
        self.labelRoot.SetLabel(self.generator.tone_name(self.spinCtrlRoot.GetValue()))
        self.labelBasetone1.SetLabel(self.generator.tone_name(self.spinCtrlBasetone1.GetValue()))
        self.labelBasetone2.SetLabel(self.generator.tone_name(self.spinCtrlBasetone2.GetValue()))
        self.labelBasetone3.SetLabel(self.generator.tone_name(self.spinCtrlBasetone3.GetValue()))
        self.labelSpeed.SetLabel("%d" % self.sliderSpeed.GetValue())
        self.labelVolume.SetLabel("%d%%" % self.sliderVolume.GetValue())
        self.labelSigma.SetLabel("%.2f" % (self.sliderSigma.GetValue() / 100.))
        self.labelBackshift.SetLabel("%.2f" % (self.sliderBackshift.GetValue() / 100.))
        self.labelSawDetune.SetLabel("%.2f" % (self.sliderToneSawDetune.GetValue() / 100.))
        self.labelSawBalance.SetLabel("%.2f" % (self.sliderToneSawBalance.GetValue() / 100.))
        self.labelSumRatio.SetLabel("%.2f" % (self.sliderToneSumRatio.GetValue() / 100.))
        self.labelSumIndex.SetLabel("%.2f" % (self.sliderToneSumIndex.GetValue() / 100.))
        self.labelHarmonizeDist.SetLabel(self.generator.tone_name(self.spinCtrlHarmonizeDist.GetValue()))
        self.labelHarmonizeFeedback.SetLabel("%.2f" % (self.sliderHarmonizeFeedback.GetValue() / 100.))
        self.labelHarmonizeWindow.SetLabel("%.2f" % (self.sliderHarmonizeWindow.GetValue() / 100.))
        self.labelDistortDrive.SetLabel("%.2f" % (self.sliderDistortDrive.GetValue() / 100.))
        self.labelDistortSlope.SetLabel("%.2f" % (self.sliderDistortSlope.GetValue() / 100.))
        self.labelChorusDepth.SetLabel("%.2f" % (self.sliderChorusDepth.GetValue() / 100.))
        self.labelChorusFeedback.SetLabel("%.2f" % (self.sliderChorusFeedback.GetValue() / 100.))
        self.labelChorusBalance.SetLabel("%.2f" % (self.sliderChorusBalance.GetValue() / 100.))
        self.labelReverbSize.SetLabel("%.2f" % (self.sliderReverbSize.GetValue() / 100.))
        self.labelReverbDamp.SetLabel("%.2f" % (self.sliderReverbDamp.GetValue() / 100.))
        self.labelReverbBalance.SetLabel("%.2f" % (self.sliderReverbBalance.GetValue() / 100.))
        self.labelDelayTime.SetLabel("%.2f" % (self.sliderDelayTime.GetValue() / 100.))
        self.labelDelayFeedback.SetLabel("%.2f" % (self.sliderDelayFeedback.GetValue() / 100.))
        self.labelToneRangeLow.SetLabel("-%d" % self.sliderToneRange.GetValue())
        self.labelToneRangeHigh.SetLabel("+%d" % self.sliderToneRange.GetValue())

        if not self.generator.check_scale_interval(self.spinCtrlBasetone1.GetValue()):
            self.labelBasetone1.SetForegroundColour('#FF0000')
        else:
            self.labelBasetone1.SetForegroundColour('')
        if not self.generator.check_scale_interval(self.spinCtrlBasetone2.GetValue()):
            self.labelBasetone2.SetForegroundColour('#FF0000')
        else:
            self.labelBasetone2.SetForegroundColour('')
        if not self.generator.check_scale_interval(self.spinCtrlBasetone3.GetValue()):
            self.labelBasetone3.SetForegroundColour('#FF0000')
        else:
            self.labelBasetone3.SetForegroundColour('')
        if not self.generator.check_scale_interval(self.spinCtrlHarmonizeDist.GetValue()):
            self.labelHarmonizeDist.SetForegroundColour('#FF0000')
        else:
            self.labelHarmonizeDist.SetForegroundColour('')

    def setChannels(self, event):
        channel1 = self.spinCtrlBasetone1.GetValue()
        channel2 = self.spinCtrlBasetone2.GetValue()
        channel3 = self.spinCtrlBasetone3.GetValue()

        channels = OrderedDict()
        if self.checkboxChannel1.GetValue():
            channels[1] = channel1
        if self.checkboxChannel2.GetValue():
            channels[2] = channel2
        if self.checkboxChannel3.GetValue():
            channels[3] = channel3
        if hash(frozenset(channels.items())) != hash(frozenset(self.generator.channels.items())):
            self.generator.setChannels(channels)
        self.updateLabels()

    def setOutputs(self, event):
        outputs = self.spinCtrlOutputs.GetValue()
        if outputs != self.generator.outputs:
            self.generator.setOutputs(outputs)

    def setScale(self, event):
        index = self.choiceScale.GetSelection()
        if index == wx.NOT_FOUND:
            return
        name = self.choiceScale.GetString(index).lower()
        if name != self.generator.scalename:
            self.generator.setScale(name)
            self.drawTone(None)
        self.updateLabels()

    def setRandomizer(self, event):
        index = self.choiceRandomizer.GetSelection()
        if index == wx.NOT_FOUND:
            return
        name = self.choiceRandomizer.GetString(index).lower()
        if name != self.generator.randomizer:
            self.generator.setRandomizer(name)
        self.updateLabels()

    def setRoot(self, event):
        root = self.spinCtrlRoot.GetValue()
        if root != self.generator.root:
            self.generator.setRoot(root)
        self.updateLabels()

    def setSpeed(self, event):
        speed = self.sliderSpeed.GetValue()
        if speed != self.generator.speed:
            self.generator.setSpeed(speed)
        self.updateLabels()

    def setVolume(self, event):
        volume = self.sliderVolume.GetValue() / 100.
        if volume != self.generator.volume:
            self.generator.setVolume(volume)
        self.updateLabels()

    def setSigma(self, event):
        sigma = self.sliderSigma.GetValue() / 100.
        if sigma != self.generator.sigma:
            self.generator.setSigma(sigma)
        self.updateLabels()

    def setBackshift(self, event):
        backshift = self.sliderBackshift.GetValue() / 100.
        if backshift != self.generator.backshift:
            self.generator.setBackshift(backshift)
        self.updateLabels()

    def setTone(self, event):
        toneSine    = self.radioButtonToneSine.GetValue()
        toneSaw     = self.radioButtonToneSaw.GetValue()
        toneSum     = self.radioButtonToneSum.GetValue()
        sawDetune   = self.sliderToneSawDetune.GetValue() / 100.
        sawBalance  =  self.sliderToneSawBalance.GetValue() / 100.
        sumRatio    = self.sliderToneSumRatio.GetValue() / 100.
        sumIndex    =  self.sliderToneSumIndex.GetValue() / 100.

        if toneSine:
            self.generator.setToneSine()
        else:
            self.generator.setToneSaw(toneSaw, sawDetune, sawBalance)
            self.generator.setToneSum(toneSum, sumRatio, sumIndex)
        self.updateLabels()

    def setEffects(self, event):
        harmonize           = self.checkboxHarmonize.GetValue()
        harmonizeDist       = self.spinCtrlHarmonizeDist.GetValue()
        harmonizeFeedback   = self.sliderHarmonizeFeedback.GetValue() / 100.
        harmonizeWindow     = self.sliderHarmonizeWindow.GetValue() / 100.
        distort             = self.checkboxDistort.GetValue()
        distortDrive        = self.sliderDistortDrive.GetValue() / 100.
        distortSlope        = self.sliderDistortSlope.GetValue() / 100.
        chorus              = self.checkboxChorus.GetValue()
        chorusDepth         = self.sliderChorusDepth.GetValue() / 100.
        chorusFeedback      = self.sliderChorusFeedback.GetValue() / 100.
        chorusBalance       = self.sliderChorusBalance.GetValue() / 100.
        reverb              = self.checkboxReverb.GetValue()
        reverbSize          = self.sliderReverbSize.GetValue() / 100.
        reverbDamp          = self.sliderReverbDamp.GetValue() / 100.
        reverbBalance       = self.sliderReverbBalance.GetValue() / 100.
        delay              = self.checkboxDelay.GetValue()
        delayTime           = self.sliderDelayTime.GetValue() / 100.
        delayFeedback       = self.sliderDelayFeedback.GetValue() / 100.

        self.generator.setHarmonize(harmonize, harmonizeDist, harmonizeFeedback, harmonizeWindow)
        self.generator.setDistort(distort, distortDrive, distortSlope)
        self.generator.setChorus(chorus, chorusDepth, chorusFeedback, chorusBalance)
        self.generator.setReverb(reverb, reverbSize, reverbDamp, reverbBalance)
        self.generator.setDelay(delay, delayTime, delayFeedback)
        self.updateLabels()

    def setStaccato(self, event):
        enabled = self.checkboxStaccato.GetValue()
        if enabled != self.generator.staccato:
            self.generator.setStaccato(enabled)
        self.updateLabels()

    def setShuffle(self, event):
        enabled = self.checkboxShuffle.GetValue()
        if enabled != self.generator.shuffle:
            self.generator.setShuffle(enabled)
        self.updateLabels()

    def setToneRange(self, event):
        tonerange = self.sliderToneRange.GetValue()
        if tonerange != self.generator.tonerange:
            self.generator.setToneRange(tonerange)
        self.updateLabels()

    def draw(self, event):
        if self.generator.count == self.toneCount:
            return

        self.drawCurrentTone(event)
        self.drawTone(event)
        self.drawHistogram(event)
        self.drawFlow(event)

        self.toneCount = self.generator.count

    def drawCurrentTone(self, event):
        BGCOLOR  = '#000000'
        FGCOLOR  = '#FFFFFF'
        BORDER   = 2

        tones    = self.generator.tones
        channels = self.generator.channels

        width, height = self.panelCurrentTone.GetSizeTuple()
        dc = wx.BufferedDC(wx.ClientDC(self.panelCurrentTone))

        #dc.BeginDrawing()

        dc.SetBrush(wx.Brush(BGCOLOR))
        dc.DrawRectangle(0, 0, width, height)

        width  -= 2 * BORDER
        height -= 2 * BORDER

        if len(channels) > 0:
            xdelta = int(width / 3)
            xoffset = BORDER + int((width % 3) / 2)  # shift
            for i in channels.keys():  # 1..3
                xpos = xoffset + (i-1) * xdelta

                label = self.generator.tone_name(tones[i])
                dc.SetFont(wx.Font(14, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD))
                w, h = dc.GetTextExtent(label)
                dc.SetTextForeground(FGCOLOR)
                dc.DrawText(label, xpos + 1 + (xdelta-w)/2, BORDER + (height-h)/2)

        #dc.EndDrawing()

    def drawTone(self, event):
        BGCOLOR  = '#000000'
        FGCOLOR  = '#FFFFFF'
        FGCOLOR0 = '#FF0000'
        FGCOLOR3 = '#FFFF00'
        FGCOLOR5 = '#00FF00'
        FGCOLORX = '#1F1F1F'
        BORDER   = 2

        intervals = [ t % 12 for t in self.generator.tones.values() ]

        width, height = self.panelTone.GetSizeTuple()
        dc = wx.BufferedDC(wx.ClientDC(self.panelTone))

        #dc.BeginDrawing()

        dc.SetBrush(wx.Brush(BGCOLOR))
        dc.DrawRectangle(0, 0, width, height)

        width  -= 2 * BORDER
        height -= 2 * BORDER

        xdelta = int(width / 12)
        xoffset = BORDER + int((width % 12) / 2)  # shift
        for i in range(len(self.generator.INTERVALS)):
            num = min(intervals.count(i), 3)

            xpos = xoffset + i * xdelta

            label = self.generator.INTERVALS[i].text
            if num > 0:
                if label == "1":  # root
                    dc.SetBrush(wx.Brush(FGCOLOR0))
                elif label == "3":  # third
                    dc.SetBrush(wx.Brush(FGCOLOR3))
                elif label == "5":  # fifth
                    dc.SetBrush(wx.Brush(FGCOLOR5))
                else:
                    dc.SetBrush(wx.Brush(FGCOLOR))
            else:
                if self.generator.check_scale_interval(i):
                    dc.SetBrush(wx.Brush(FGCOLORX))
                else:
                    continue
            dc.DrawRectangle(xpos + 1, BORDER, xdelta - 2, height)

            if num == 2:
                label = "²" + label  # played twice
            elif num == 3:
                label = "³" + label  # played thrice
            dc.SetFont(wx.Font(10, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD))
            w, h = dc.GetTextExtent(label)
            dc.SetTextForeground(BGCOLOR)
            dc.DrawText(label, xpos + 1 + (xdelta-w)/2, BORDER + (height-h)/2)

        #dc.EndDrawing()

    def drawFlow(self, event):
        BGCOLOR  = '#000000'
        FGCOLOR  = '#FFFFFF'
        BORDER   = 2

        channels  = self.generator.channels
        flowrange = self.generator.tonerange
        history   = self.generator.history[:]  # make copy before reversing!
        history.reverse()

        width, height = self.panelFlow.GetSizeTuple()
        dc = wx.BufferedDC(wx.ClientDC(self.panelFlow))

        #dc.BeginDrawing()

        dc.SetBrush(wx.Brush(BGCOLOR))
        dc.DrawRectangle(0, 0, width, height)

        width  -= 2 * BORDER
        height -= 2 * BORDER

        if len(history) > 0:
            for k in range(min(len(history),height)):
                for c in channels.keys():  # 1..3
                    dc.SetPen(wx.Pen(FGCOLOR))
                    t = int(history[k][c])
                    if t < -flowrange:
                        t = -flowrange
                    if  t > flowrange:
                        t = flowrange
                    t += flowrange

                    length = int(t / float(2*flowrange) * (width/3.))
                    ypos = height - k
                    if c == 1:
                        xpos = BORDER
                        dc.DrawLine(xpos, ypos, length, ypos)
                    elif c == 2:
                        xpos = int((width - length) / 2)
                        dc.DrawLine(xpos, ypos, xpos + length, ypos)
                    elif c == 3:
                        xpos = width
                        dc.DrawLine(xpos, ypos, xpos - length, ypos)

        #dc.EndDrawing()

    def drawHistogram(self, event):
        BGCOLOR  = '#000000'
        FGCOLOR  = '#FFFFFF'
        FGCOLOR0 = '#FF0000'
        FGCOLOR3 = '#FFFF00'
        FGCOLOR5 = '#00FF00'
        BORDER   = 2

        histrange = self.generator.tonerange
        histogram = self.generator.histogram

        width, height = self.panelHistogram.GetSizeTuple()
        dc = wx.BufferedDC(wx.ClientDC(self.panelHistogram))

        #dc.BeginDrawing()

        dc.SetBrush(wx.Brush(BGCOLOR))
        dc.DrawRectangle(0, 0, width, height)

        width  -= 2 * BORDER
        height -= 2 * BORDER

        if len(histogram) > 0:
            maxval = max(histogram.values())

            xdelta  = int(width / (2*histrange))
            xoffset = BORDER + int(width % (2*histrange) / 2)  # shift
            for t in sorted(histogram.keys()):
                t = int(t)
                if t < -histrange or t > histrange:
                    continue
                xpos = xoffset + (t + histrange) * xdelta

                value = histogram[t]
                length = int(float(value) / maxval * height)

                label = self.generator.INTERVALS[t % 12].text
                if label == "1":  # root
                    dc.SetBrush(wx.Brush(FGCOLOR0))
                elif label == "3":  # third
                    dc.SetBrush(wx.Brush(FGCOLOR3))
                elif label == "5":  # fifth
                    dc.SetBrush(wx.Brush(FGCOLOR5))
                else:
                    dc.SetBrush(wx.Brush(FGCOLOR))
                dc.DrawRectangle(xpos, height - length, xdelta, length)

        #dc.EndDrawing()
