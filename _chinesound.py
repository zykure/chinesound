#!/usr/bin/env python2
# -*- coding: utf8 -*-


import os
import time
import math
import random
import string
import threading
import pyo
import numpy
import xml.etree.ElementTree as ET

from collections import namedtuple, OrderedDict

######################################################################

Interval = namedtuple('Interval', ['text', 'diff', 'sign'])

SCALES = OrderedDict()
RANDOMIZERS = OrderedDict()

class SoundServer:

    def __init__(self):
        self.server = None

    def boot(self, device=None):
        if self.server:
            self.server.stop()

        self.server = pyo.Server()
        if device >= 0:
            self.server.setOutputDevice(device)
        self.server.boot()
        self.server.start()

class ToneGenerator(threading.Thread):

    ROOT_TONE   = 440

    INTERVALS = {
        0:  Interval("1",  1,  0),
        1:  Interval("b2", 2, -1),
        2:  Interval("2",  2,  0),
        3:  Interval("b3", 3, -1),
        4:  Interval("3",  3,  0),
        5:  Interval("4",  4,  0),
        6:  Interval("b5", 5, -1),
        7:  Interval("5",  5,  0),
        8:  Interval("#5", 5, +1),
        9:  Interval("6",  6,  0),
        10: Interval("b7", 7, -1),
        11: Interval("7",  7,  0),
    }

    NAMES = [ "a", "ais", "b", "c", "cis", "d", "dis", "e", "f", "fis", "g", "gis" ]

    CHANNELMAP = {
    #    1:  [ (1.0, 1.0) ],
    #    2:  [ (0.8, 0.2), (0.2, 0.8) ],
    #    3:  [ (0.6, 0.0), (0.4, 0.4), (0.0, 0.6) ],
        1:  [ (1.0, 1.0) ],
        2:  [ (1.0, 0.2), (0.2, 1.0) ],
        3:  [ (1.0, 0.0), (0.5, 0.5), (0.0, 1.0) ],
    }


    # CHANNELMAP[outputs][channels][inchannel][outchannel] = volume
    CHANNELMAP = {
        1:  {  # 1 output channel - mono
                1:  [ [1.0] ],
                2:  [ [1.0], [1.0] ],
                3:  [ [1.0], [1.0], [1.0] ],
            },
        2:  {  # 2 output channels - left,right
                1:  [ [1.0,1.0] ],
                2:  [ [1.0,0.2], [0.2,1.0] ],
                3:  [ [1.0,0.0], [1.0,1.0], [0.0,1.0] ],
            },
        3:  {  # 3 output channels - left,right,center
                1:  [ [1.0,0.8,1.0] ],
                2:  [ [1.0,0.2,0.4], [0.2,1.0,0.4] ],
                3:  [ [1.0,0.0,0.2], [0.2,0.2,1.0], [0.0,1.0,1.2] ],
            },
        4:  {  # 4 output channels - left,right,backleft,backright
                1:  [ [1.0,1.0,0.8,0.8] ],
                2:  [ [1.0,0.2,0.8,0.2], [0.2,1.0,0.2,0.8] ],
                3:  [ [1.0,0.0,0.8,0.2], [1.0,1.0,0.4,0.4], [0.0,1.0,0.2,0.8] ],
            },
        5:  {  # 5 output channels - left,right,center,backleft,backright
                1:  [ [1.0,1.0,0.8,0.8,0.8] ],
                2:  [ [1.0,0.2,0.4,0.8,0.2], [0.2,1.0,0.4,0.2,0.8] ],
                3:  [ [1.0,0.0,0.2,0.8,0.2], [0.4,0.4,1.0,0.2,0.2], [0.2,1.0,0.2,0.2,0.8] ],
            }
    }

    def __init__(self):
        self.loadXML()

        self.root = 0  # offset 0 = a = A4 = 440 Hz

        self.randomizer = RANDOMIZERS.keys()[0]  # first

        self.scalename = None
        self.scale = None
        self.tonerange = 24

        self.channels = { 1: 0 }  # channel: basetone
        self.outputs  = 2

        self.speed = 120
        self.volume = 0.8

        self.sigma = 2
        self.backshift = 0.1

        self.tones = {}
        self.sources = {}
        self.mixer = pyo.Mixer()

        self.toneSaw = False
        self.toneSawDetune = 0.5
        self.toneSawBalance = 0.7

        self.toneSum = False
        self.toneSumRatio = 0.5
        self.toneSumIndex = 0.5

        self.staccato = False
        self.shuffle = False

        self.harmonize = False
        self.harmonizeDist = -7
        self.harmonizeFeedback = 0
        self.harmonizeWindow = 0.1

        self.distort = False
        self.distortDrive = 0.75
        self.distortSlope = 0.5

        self.chorus = False
        self.chorusDepth = 1
        self.chorusFeedback = 0.25
        self.chorusBalance = 0.5

        self.reverb = False
        self.reverbSize = 0.5
        self.reverbDamp = 0.5
        self.reverbBalance = 0.5

        self.delay = False
        self.delayTime = 0.25
        self.delayFeedback = 0

        self.count = 0
        self.histogram = {}
        self.history   = []

        self.paused = False
        self.playing = False
        self.doUpdate = True
        self.fullUpdate = True

        self.lock = threading.Lock()
        threading.Thread.__init__(self)

    def loadXML(self):
        basedir = os.path.dirname(__file__)

        tree = ET.parse(basedir + '/scales.xml')
        for elem in tree.getroot().findall('scale'):
            name = elem.attrib['name']
            keys = [ int(i) - 1 for i in elem.attrib['keys'].split(',') ]  # we're using interval range [0,11]
            SCALES[name.lower()] = keys

        tree = ET.parse(basedir + '/randomizers.xml')
        for elem in tree.getroot().findall('randomizer'):
            name = elem.attrib['name']
            func = elem.attrib['function']
            RANDOMIZERS[name.lower()] = func

        print "Loaded %d scales and %d randomizers from XML configs" % (len(SCALES), len(RANDOMIZERS))

    def run(self):
        self.playing = True
        while self.playing:
            while self.paused:
                self.mute()
                self.mixer.stop()
                time.sleep(0.1)

            if self.doUpdate:
                self.update()

            self.lock.acquire()

            tones = {}

            print "tone %6d:" % self.count,
            for c in self.channels.keys():
                t = self.tones[c]
                freq = self.tone2freq(t)

                s = self.sources[c]
                s.setFreq(freq)
                if not (self.toneSaw or self.toneSum):
                    s.setPhase(0)

                if not self.histogram.has_key(t):
                    self.histogram[t] = 0
                self.histogram[t] += 1

                tones[c] = t
                print "  %-6s (%+2.2d:  %4.0f Hz)" % (self.tone_name(t), t, self.tone2freq(t)),
            print ""

            self.history.append(tones)
            self.unmute()

            for c in self.channels.keys():
                self.tones[c] = self.next_tone(self.tones[c], self.channels[c])

            length = self.bpm2length(self.speed)
            if self.shuffle:
                if self.count % 2 == 0:
                    length *= 1.333
                else:
                    length *= 0.667
            if self.staccato:
                time.sleep(length/2)
                self.mute()
                time.sleep(length/2)
            else:
                time.sleep(length)

            self.count += 1

            self.lock.release()

    def mute(self):
        for s in self.sources.values():
            s.stop()

    def unmute(self):
        self.mixer.out()
        for s in self.sources.values():
            s.out()

    def update(self):
        self.lock.acquire()

        print "Upating ..."

        self.mute()
        self.mixer.stop()
        self.mixer = pyo.Mixer()

        if len(self.channels) > 0:
            self.sources = {}
            for c in self.channels.keys():
                if self.toneSaw:
                    s = pyo.SuperSaw(0, detune=self.toneSawDetune, bal=self.toneSawBalance)
                elif self.toneSum:
                    s = pyo.SumOsc(0, ratio=self.toneSumRatio, index=self.toneSumIndex)
                else:
                    s = pyo.Sine(0)
                s.setMul(self.volume)
                self.sources[c] = s

            volmap = self.CHANNELMAP[self.outputs][max(self.channels)]
            for c,s in self.sources.items():
                o = s
                if self.harmonize:
                    o = o + pyo.Harmonizer(o, transpo=self.harmonizeDist, feedback=self.harmonizeFeedback, winsize=self.harmonizeWindow)
                if self.distort:
                    o =  pyo.Disto(o, drive=self.distortDrive, slope=self.distortSlope)
                if self.chorus:
                    o = pyo.Chorus(o, depth=self.chorusDepth, feedback=self.chorusFeedback, bal=self.chorusBalance)
                if self.reverb:
                    o = pyo.Freeverb(o, size=self.reverbSize, damp=self.reverbDamp, bal=self.reverbBalance)
                if self.delay:
                    o = pyo.Delay(o, delay=self.delayTime, feedback=self.delayFeedback)

                channel = c - 1
                self.mixer.addInput(channel, o)
                for n in range(self.outputs):
                    self.mixer.setAmp(channel, n, volmap[channel][n])

        if self.fullUpdate:
            self.tones = self.channels.copy()  # contains basetones as values
            self.count = 0
            self.history = []
            self.histogram = {}
            self.fullUpdate = False

        self.doUpdate = False
        self.lock.release()

    def clear(self):
        self.fullUpdate = True
        self.doUpdate = True

    def stop(self):
        self.playing = False

    def pause(self):
        self.paused = True

    def play(self):
        self.paused = False

    def setToneRange(self, tonerange):
        self.tonerange = tonerange

    def setChannels(self, channels):
        self.channels = channels
        self.fullUpdate = True
        self.doUpdate = True

    def setOutputs(self, outputs):
        self.outputs = outputs
        self.fullUpdate = True
        self.doUpdate = True

    def setScale(self, name):
        if SCALES.has_key(name):
            self.scalename = name
            self.scale = SCALES[name]

    def setRandomizer(self, name):
        if RANDOMIZERS.has_key(name):
            self.randomizer = name

    def setRoot(self, root):
        self.root = root

    def setSpeed(self, bpm):
        self.speed = bpm

    def setVolume(self, volume):
        self.volume = volume
        #self.doUpdate = True
        for s in self.sources.values():
            s.setMul(self.volume)

    def setStaccato(self, enable=True):
        self.staccato = enable

    def setShuffle(self, enable=True):
        self.shuffle = enable

    def setSigma(self, sigma):
        self.sigma = sigma

    def setBackshift(self, backshift):
        self.backshift = backshift

    def setToneSine(self):
        self.toneSaw = False
        self.toneSum = False
        self.doUpdate = True

    def setToneSaw(self, enable=True, detune=0.5, balance=0.7):
        if enable:
            self.toneSum = False
            self.toneSaw = True
        self.toneSawDetune = detune
        self.toneSawBalance = balance
        self.doUpdate = True

    def setToneSum(self, enable=True, ratio=0.5, index=5):
        if enable:
            self.toneSaw = False
            self.toneSum = True
        self.toneSumRatio = ratio
        self.toneSumIndex = index
        self.doUpdate = True

    def setHarmonize(self, enable=True, dist=-7, feedback=0, window=0.1):
        self.harmonize = enable
        self.harmonizeDist = dist
        self.harmonizeFeedback = feedback
        self.harmonizeWindow = window
        self.doUpdate = True

    def setDistort(self, enable=True, drive=0.75, slope=0.5):
        self.distort = enable
        self.distortDrive = drive
        self.distortSlope = slope
        self.doUpdate = True

    def setChorus(self, enable=True, depth=1, feedback=0.25, balance=0.5):
        self.chorus = enable
        self.chorusDepth = depth
        self.chorusFeedback = feedback
        self.chorusBalance = balance
        self.doUpdate = True

    def setReverb(self, enable=True, size=0.5, damp=0.5, balance=0.5):
        self.reverb = enable
        self.reverbSize = size
        self.reverbDamp = damp
        self.reverbBalance = balance
        self.doUpdate = True

    def setDelay(self, enable=True, time=0.25, feedback=0):
        self.delay = enable
        self.delayTime = time
        self.delayFeedback = feedback
        self.doUpdate = True

    def check_scale(self, tone):
        if not self.scale:
            return True
        if len(self.scale) <= 1:
            return True
        t = int(self.root + tone)
        if t % 12 in self.scale:
            return True
        return False

    def check_scale_interval(self, interval):
        if not self.scale:
            return True
        if len(self.scale) <= 1:
            return True
        if interval % 12 in self.scale:
            return True
        return False

    def next_tone(self, tone, basetone):
        delta = tone - basetone
        mean  = -1 * self.backshift * delta
        sign  = random.choice([-1,1])
        shift = round(eval(RANDOMIZERS[self.randomizer].format(mean, self.sigma, sign)))
        tone += shift
        while not self.check_scale_interval(tone):
            if tone > basetone:
                tone += 1
            else:
                tone -= 1
        if tone < (self.root-self.tonerange) or tone > (self.root+self.tonerange):
            tone = basetone
        return tone

    def tone2freq(self, tone):
        t = int(self.root + tone)
        return self.ROOT_TONE * math.exp(t/12.0)

    def tone2interval(self, tone):
        t = int(self.root + tone)
        return self.INTERVALS[t % 12]

    def tone_name(self, tone):
        t = int(self.root + tone)
        delta = t + 9  # c is root
        shift = int(delta / 12)
        if shift < 0:
            name = self.NAMES[t % 12]
            name += "," * abs(shift)
        else:
            name = self.NAMES[t % 12]
            name += "'" * shift
        return name

    @staticmethod
    def bpm2length(bpm):
        return 60.0 / bpm
